#!/bin/sh

echo 'VERBOSE_OUTPUT=no
ACTIVE_CONSOLES="/dev/tty[1-6]"

CHARMAP="UTF-8"

CODESET="guess"
FONTFACE="TerminusBold"
FONTSIZE="8x16"

VIDEOMODE=

XKBMODEL="pc105"
XKBLAYOUT="us,ru"
XKBVARIANT=",winkeys"
XKBOPTIONS="grp:ctrl_shift_toggle,grp:rwin_switch,lv3:ralt_switch,grp_led:scroll"
 '> /etc/default/console-setup

echo 'XKBMODEL="pc105"
XKBLAYOUT="us,ru"
XKBVARIANT=","
XKBOPTIONS="grp:ctrl_shift_toggle,grp:rwin_switch,lv3:ralt_switch,grp_led:scroll"

BACKSPACE="guess"'> /etc/default/keyboard

echo 'style ter-uni-framebuf
size 16
encoding utf-8
layout ru
options ctrl_shift_toggle win_switch
ttys /dev/tty[1-6]"'> /etc/console-cyrillic
