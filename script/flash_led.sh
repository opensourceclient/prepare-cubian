#/bin/bash 
DOT=.
DASH=-
LOCK=/var/lock//flash_led.lock
LED=
#BLUE_LED=/sys/class/leds/blue\:ph21\:led2/
#GREEN_LED=/sys/class/leds/green\:ph20\:led1/

if [ -f $LOCK ];then
    echo "morse message queue is full"
    exit 1
fi

touch $LOCK



# # Fix GPIO permission issue, gpio interface belongs to root by default.
# инициализация LED_READY
/bin/echo 1 > /sys/class/gpio/export
/bin/echo out > /sys/devices/platform/gpio-sunxi/gpio/gpio1_ph12/direction > /dev/null 2>&1
chgrp -R gpio /sys/devices/platform/gpio-sunxi/gpio
chmod -R g+rw /sys/devices/platform/gpio-sunxi/gpio
find /sys/devices/platform/gpio-sunxi/gpio -maxdepth 1 -follow -type d | sed "s/\$/\//" | xargs chgrp -R gpio
find /sys/devices/platform/gpio-sunxi/gpio -maxdepth 1 -follow -type d | sed "s/\$/\//" | xargs chmod -R g+rw






# init led stat
echo 0 > ${BLUE_LED}/brightness

blink(){
    echo 1 > ${BLUE_LED}/brightness
    sleep $1
    echo 0 > ${BLUE_LED}/brightness
    sleep 1
}

CODES=(`/usr/lib/cubian/cubian-morseencode $1`)
for CODE in "${CODES[@]}";do
    ATOMS=($(echo $CODE | sed 's/\(.\)/\1 /g'))
    for ATOM in "${ATOMS[@]}";do
        if [ "$ATOM" = $DOT ];then
            #echo "dot"
            blink 0.5
        elif [ "$ATOM" = $DASH ];then
            #echo "dash"
            blink 1
        else
            echo "morse message decode error"
            rm -f $LOCK
            exit 2
        fi
    done
    sleep 2
    #echo "end of one letter"
done

rm -f $LOCK
