. ./prepare-cubian/color_out.sh

echoYellow "###################################################################"
echoGreen "Install kernel"

cd ~
sudo cp -v /boot/uImage /boot/uImage.work
sudo cp -v prepare-cubian/deb/uImage /boot/
sudo dpkg -i prepare-cubian/deb/linux-image-3.4.79-otk110-20150512-sun4i_3_armhf.deb

echoRed "Shutdown now"
sudo halt -p

