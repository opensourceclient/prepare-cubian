Скрипты, которые позволяют обновить [Cubian](http://cubian.org/)  (дистрибутив для плат на [Soc Allwinner](http://www.allwinnertech.com/en/) [A10](http://linux-sunxi.org/A10)/[A20](http://linux-sunxi.org/A20)) до актуальной версии Debian armhf. После запуска каждого необходима перезагрузка.
После выполнения всех скриптов будет установлена актуальная версия Debian.

Скрипты проверялись на плате с Soc Allwinner A10.

Обновление с версии Cubian-nano-x1-a10-hdmi до Debian 8.3 (актуальной на момент "сейчас"). С минимальными изменениями (а может и без них) должно работать и на A20.

Дистрибутивы [Cubian](http://cubian.org/) предназначены для плат на основе [Soc Allwinner](http://www.allwinnertech.com/en/) [A10](http://linux-sunxi.org/A10)/[A20](http://linux-sunxi.org/A20). 
Скрипты были проверены на Allwinner A10 на плате [DS-110](https://opensourceclient.org/10-faq/16-specifications-ds-110), которая является расширенным аналогом [CubieBoard](http://cubieboard.org/). 

### Обновить пакеты и установить git ###
```
apt-get install debian-keyring debian-archive-keyring
apt-get update
apt-get install git

```

### Получить файлы из репозитория ###
```
#!bash

mkdir git
cd git/
git clone https://opensourceclient@bitbucket.org/opensourceclient/prepare-cubian.git
```

## Первый скрипт: ##
добавляем удобства (файлы .nanorc, .bashr), устанавливаем ограничение частоты(через cpufrequtils), перенаправляем логи на ram-диск, отключаем IPv6.
```
./prepare-cubian/cubian_update-1.sh
```

Плата будет выключена.
## Второй скрипт: ##
```
./prepare-cubian/cubian_update-2.sh
```

удаляем неиспользуемые пакеты, добавляем локализацию (необходимо выбрать раскладку клавиатуры и способ переключения, выбрать шрифт для консоли и так несколько раз), обновляем пакеты до текущих версий
Плата будет выключена.
## Третий скрипт: ##
Обновление Debian с версии 7.1 до 8
```
./prepare-cubian/cubian_update-3.sh
```

## Оболочка для использования платы в качестве терминального клиента RDP ##
```
./prepare-cubian/cubian_update-customize.sh
```
# Опционально #
## Установить ядро ##
```
./prepare-cubian/cubian_update-kernel.sh
```
## Установка средств разработки ##
```
./prepare-cubian/cubian_update-development.sh
```