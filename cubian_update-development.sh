echo "###################################################################"
echo "Средства разработки"
sudo apt-get install -y emacs
sudo apt-get install -y git  make gcc build-essential fakeroot usbutils libcurl4-openssl-dev libc6-dev libpcsclite-dev libgsm1-dev
sudo apt-get install -y libnewt-dev libconfig8-dev
sudo apt-get install -y cmake
sudo apt-get install -y checkinstall


echo "Для сборки FreeRDP"
sudo apt-get install -y libgstreamer-plugins-base0.10-dev libx11-dev libxcursor-dev libasound2 libjpeg-dev
sudo apt-get install -y build-essential git-core cmake libssl-dev libxext-dev libxinerama-dev
sudo apt-get install -y libasound2-dev
sudo apt-get install -y libxdamage-dev libxv-dev libxkbfile-dev libasound2-dev libxml2 libxml2-dev libxrandr-dev libgstreamer0.10-dev
sudo apt-get install -y libcups2-dev
sudo apt-get install -y libxinerama-dev libpcsclite-dev
sudo apt-get install -y libxrender-dev libxv-dev
sudo apt-get install -y libxcursor-dev libxi-dev
sudo apt-get install -y gstreamer-1.0
sudo apt-get install -y libavutil-dev libavcodec-dev
sudo apt-get install -y libgsm1-dev
sudo apt-get install -y libgstreamer0.10-dev libxi-dev libgstreamer-plugins-base1.0-dev
sudo apt-get install -y libdirectfb-dev libglib2.0-dev

###sudo apt-get install -y gstreamer0.10-ffmpeg gstreamer0.10-ffmpeg-dbg
sudo apt-get install -y libavcodec-extra-56

echo "pulseaudio"
sudo apt-get install -y libpulse-dev
sudo apt-get install -y pulseaudio


echo "Перенаправление USB"
sudo apt-get install -y libusb-dev
sudo apt-get install -y libusb-1.0-0-dev
sudo apt-get install -y uuid-dev
sudo apt-get install -y libdbus-glib-1-dev
sudo apt-get install -y dbus-*-dev
sudo apt-get install -y libudev-dev

sudo halt -p
