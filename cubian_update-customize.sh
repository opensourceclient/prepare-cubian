. ./prepare-cubian/color_out.sh

echoYellow "###################################################################"
echoGreen "set time via NTP"
sudo ntpdate-debian
sudo apt-get update -y

echoYellow "###################################################################"
echoGreen "  Install add.packages"

sudo apt-get install -y pcsc-tools libpcsclite1 pcscd libccid
sudo apt-get install -y consolekit
sudo apt-get install -y pulseaudio
sudo apt-get install -y miscfiles
sudo apt-get install -y fbi
#sudo apt-get install -y --force-yes libump
sudo apt-get install -y gstreamer1.0-plugins-base
sudo apt-get install -y libgstreamer1.0-0
sudo apt-get install -y libgstreamer-plugins-base1.0-0

# Установка Xorg
echoYellow "###################################################################"
echoGreen "  Install Xorg"
sudo apt-get install -y xserver-xorg-core xinit
sudo apt-get install -y libsunxi-mali-x11
sudo apt-get install -y xserver-xorg-video-sunximali sunxi-disp-test
sudo apt-get install -y xorg
sudo apt-get install -y xscreensaver
sudo usermod -a -G video cubie

#Разрешить запуск XWindow не только root
sudo cp /etc/X11/Xwrapper.config /etc/X11/Xwrapper.config.orig
sudo bash -c "cat /etc/X11/Xwrapper.config.orig \
        | sed -e 's/allowed_users=.*/allowed_users=anybody/g' > /etc/X11/Xwrapper.config"

echoYellow "###################################################################"
echo "  Install packages for setting-ds110"
sudo apt-get install -y libconfig8-dev console-cyrillic iceweasel resolvconf blackbox
sudo apt-get install -y numlockx xbindkeys x11-xserver-utils usbutils pptp-linux wicd
sudo apt-get install -y xfonts-cyrillic libglew1.10 pcscd libccid libgstreamer0.10-0 chkconfig xorg
sudo apt-get install -y cubian-nandinstall
sudo apt-get install -y --force-yes libump

echoYellow "###################################################################"
echoGreen "  Fonts"
sudo apt-get install -y fonts-liberation
sudo apt-get install -y ttf-mscorefonts-installer

echoYellow "###################################################################"
echoGreen "  Install CUPS & printer drivers"
sudo apt-get install -y --force-yes cups-pdf
sudo  apt-get install -y --force-yes printer-driver-foo2zjs
sudo mkdir -vp /usr/share/foo2zjs/icm/    \
              /usr/share/foo2oak/icm/    \
              /usr/share/foo2hp/icm/     \
              /usr/share/foo2lava/icm/   \
              /usr/share/foo2qpdl/icm/   \
              /usr/share/foo2slx/icm/    \
              /usr/share/foo2slx/icm/    \
              /usr/share/foo2hiperc/icm/ \
              /lib/firmware/hp \
              /var/run/hplip

cd /tmp && sudo getweb all
sudo  apt-get install -y libtool

sudo usermod -a -G lp,lpadmin cubie

# разрешения для папки с pdf документами
sudo sed -i '/^exit 0/imkdir -p /tmp/PDF && sudo chmod -R 777 /tmp/PDF' /etc/rc.local

#exim
#sudo bash -c "cat /dev/null > /var/log/exim4/paniclog"
rm /var/log/exim4/paniclog

sudo apt-get autoremove -y -f
sudo apt-get clean

sudo ln -s /bin/chvt /usr/bin/chvt

#wget "http://packages.opensourceclient.org/0.8.0/linux-image-3.4.79-otk110-11.03.2015-sun4i_3_armhf.deb"
#wget "http://packages.opensourceclient.org/0.8.0/uImage"

sudo bash -c "echo ds110 ALL=\(ALL\) NOPASSWD:ALL > /etc/sudoers.d/ds110"

sudo bash -c "echo 'auto lo'                        > /etc/network/interfaces"
sudo bash -c "echo '  iface lo inet loopback'       >> /etc/network/interfaces"
sudo bash -c "echo 'auto eth0'                      >> /etc/network/interfaces"
sudo bash -c "echo '  allow-hotplug eth0'           >> /etc/network/interfaces"
sudo bash -c "echo '  iface eth0 inet static'       >> /etc/network/interfaces"
sudo bash -c "echo '  address 192.168.0.174'        >> /etc/network/interfaces"
sudo bash -c "echo '  netmask 255.255.255.0'        >> /etc/network/interfaces"
sudo bash -c "echo '  gateway 192.168.0.222'        >> /etc/network/interfaces"
sudo bash -c "echo 'dns-nameservers 192.168.0.222'  >> /etc/network/interfaces"

echoYellow "###################################################################"
echoGreen "Add key ssh"
cd ~ && mkdir -p .ssh
echo "ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEAifVQDXL51cOV4lhr6k0twsMGQxLZn0PCez/PmJBjpHwSAtf+hQ/EMM2yCUtFxP+lMwmwP6s1YTMzH9Oz6vRkuB4hwo1yctG6AVEwRH6I0MwoFZ1BquzfC1MRdfQ2ArjKFjYNQoajtrdF69XJ5Wiz5/lkzUM1JUsPbk+sRMBoq55Web09oz82EJgzFvJcajktL6uUskH7nuRQ4pU7x3X16xVjilSAptfEPvnTFQ8h9KSmbMMIWg0IthsnsOGExntJNHpHnH2uh4eTDpYJiFSnkUYAC40QgNxYPxcW6MYfdU4F4qIuJxzDBnhzYBnL3QGomP7gk7YZp3MDhyhfvdbAXQ== rsa-key-20150121" > .ssh/authorized_keys
chmod 640 .ssh/authorized_keys

echoYellow "###################################################################"
echoGreen "set /etc/inittab"
sudo cp /etc/inittab /etc/inittab.orig
sudo apt-get install mingetty
sudo bash -c "cat /etc/inittab.orig \
        | sed -e '/cubian-agetty tty/s/^/#/' -e '/ca:/s/^/#/g' > /etc/inittab"

sudo bash -c "echo '# автоматический запуск на tty5 консоли утилиты управления'                    	 >> /etc/inittab"
sudo bash -c "echo '5:2345:respawn:/sbin/mingetty --autologin ds110 --noclear tty5'  		   	 >> /etc/inittab"
sudo bash -c "echo '# обычная консоль на tty6' 							   	 >> /etc/inittab"
sudo bash -c "echo '6:23:respawn:/sbin/getty 38400 tty6'					   	 >> /etc/inittab"
sudo bash -c "echo '# для выключения по ctrl+alt+del (с принудительным запуском fsck при след.загрузке)' >> /etc/inittab"
sudo bash -c "echo 'ca:12345:ctrlaltdel:/usr/local/ds110/script/shutdown_ds110.sh -t1 -F -k now'       	 >> /etc/inittab"
sudo bash -c "echo '#Для автоматического входа root на консоли serial'                             	 >> /etc/inittab"
sudo bash -c "echo 'T0:2345:respawn:/sbin/mingetty --autologin root --noclear ttyS0 115200 vt100'  	 >> /etc/inittab"
sudo bash -c "echo '#  Sleep выключает ОТК-110'                                                    	 >> /etc/inittab"
sudo bash -c "echo 'kb:12345:kbrequest:/usr/local/ds110/script/shutdown_ds110.sh -t1 -F -k now'           	 >> /etc/inittab"

sudo bash -c "echo 127.0.0.1 OTK110 >> /etc/hosts"

echoYellow "###################################################################"
echoGreen "set /etc/console-tools/remap"
sudo bash -c "echo \"s/keycode  142 = /keycode  142 = KeyboardSignal/;\" >> /etc/console-tools/remap"


echoYellow "###################################################################"
echoGreen "Install kernel"

cd ~
#sudo cp -v /boot/uImage /boot/uImage.work
#sudo cp -v prepare-cubian/deb/uImage /boot/
#sudo dpkg -i prepare-cubian/deb/linux-image-3.4.79-otk110-20150512-sun4i_3_armhf.deb

echoYellow "###################################################################"
echoGreen "Install settings-ds110"
cd ~
sudo dpkg -i prepare-cubian/deb/xf86-video-fbturbo-for-ds110_0.5.1-1_armhf.deb
sudo dpkg -i prepare-cubian/deb/freerdp-for-ds110_1.2.1-dev-1_armhf.deb
sudo dpkg -i prepare-cubian/deb/setting-ds110_0.8.2.8_armhf.deb

# for gpio
sudo cp -v prepare-cubian/script/script.bin /boot/script.bin

echoRed "Shutdown now"
sudo halt -p

