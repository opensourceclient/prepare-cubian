. ./prepare-cubian/color_out.sh

echo "###################################################################"
cat /etc/debian_version

sudo bash -c "echo cubie  ALL=\(ALL\) NOPASSWD:ALL > /etc/sudoers.d/cubie"

echo "###################################################################"
echo "get .nanorc"
cp prepare-cubian/script/.nanorc ./
sudo cp -v .nanorc /root/

echo "###################################################################"
echo "get .bashrc"
bash -c "cat prepare-cubian/script/.bashrc_add >> ~/.bashrc"
sudo bash -c "cat .bashrc_add >> /root/.bashrc"

echo "###################################################################"
echo "/etc/init.d/cpufrequtils"
sudo cp /etc/init.d/cpufrequtils /etc/init.d/cpufrequtils.bak
sudo bash -c "cat /etc/init.d/cpufrequtils.bak \
	| sed -e 's/MIN_SPEED=\".*\"/MIN_SPEED=\"800MHz\"/g' \
	| sed -e 's/MAX_SPEED=\".*\"/MAX_SPEED=\"1010MHz\"/g' > /etc/init.d/cpufrequtils \
	"

echo "###################################################################"
echo "/etc/rsyslog.conf"
sudo cp /etc/rsyslog.conf /etc/rsyslog.conf.bak
sudo bash -c "cat /etc/rsyslog.conf.bak \
        | sed -e 's/var\/log/var\/tmp/g' > /etc/rsyslog.conf"

# Отключить IPV6 http://e-kzn.ru/adminu/kak-otkliuchit-ipv6-v-debian.html
echo "disable IPv6"
sudo bash -c "echo net.ipv6.conf.all.disable_ipv6=1 > /etc/sysctl.d/disableipv6.conf"

echo "Enable check root partishion"
sudo bash -c "echo FSCKFIX=yes >> /etc/default/rcS"

sudo halt -p

